import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IWallapopCleanData} from '../table/table.component';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

  setFavorite: EventEmitter<IWallapopCleanData> = new EventEmitter<IWallapopCleanData>();

  noDataFound: boolean;
  header: string[] = ['titleFiltered', 'imageFiltered', 'favoriteFiltered'];
  dataSource: MatTableDataSource<IWallapopCleanData>;
  wallapopDataFiltered: IWallapopCleanData[];

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) data: IWallapopCleanData[]) {
    this.wallapopDataFiltered = data;
    this.dataSource = new MatTableDataSource<IWallapopCleanData>(data);
    this.dataSource.filterPredicate =
      (data: IWallapopCleanData, filter: string) => {
        return data.title.trim().toLowerCase().indexOf(filter) !== -1;
      }
  }

  ngOnInit(): void {
  }

  applyFavorite(element: IWallapopCleanData): void {
    this.setFavorite.emit(element);
  }

  searchItem(event: Event) {
    const searchValue = (event.target as HTMLInputElement).value;
    console.log(this.dataSource, searchValue)
    this.dataSource.filter = searchValue.trim().toLocaleLowerCase();
    this.noDataFound = this.dataSource && this.dataSource.filteredData && this.dataSource.filteredData.length > 0;
  }

}
