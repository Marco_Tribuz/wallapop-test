import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FavoriteComponent } from './favorite.component';
import {IWallapopCleanData} from '../table/table.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {MatInputHarness} from '@angular/material/input/testing';
import {MatTableHarness} from '@angular/material/table/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {HarnessLoader} from '@angular/cdk/testing';
import {MatButtonHarness} from '@angular/material/button/testing';

describe('FavoriteComponent', () => {
  let component: FavoriteComponent;
  let fixture: ComponentFixture<FavoriteComponent>;
  let loader: HarnessLoader;

  const data: IWallapopCleanData[] = [
    {
      title: 'iPhone 6S Oro',
      description: 'Vendo un iPhone 6 S color Oro nuevo y sin estrenar. Me han dado uno en el trabajo y no necesito el que me compré. En tienda lo encuentras por 749 euros y yo lo vendo por 740. Las descripciones las puedes encontrar en la web de apple. Esta libre.',
      email: 'iphonemail@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png',
      price: '740',
    },
    {
      description: 'Cámara clásica de fotos Polaroid, modelo 635. Las fotos son a super color. Está en perfectas condiciones y es fantástica para coleccionistas. Se necesitan carretes instax 20 para hacer fotos. Tamaño M.',
      email: 'cameramail@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/camera.png',
      price: '50',
      title: 'Polaroid 635'
    },
    {
      description: 'Vendo bolso de piel marrón grande de la marca Hoss. Lo compré hace dos temporadas. Esta en perfectas condiciones, siempre se ha guardado en bolsa de tela para su conservación. Precio original de 400 euros. Lo vendo por 250 porque ya casi no me lo pongo. Tiene varios compartimentos dentro.',
      email: 'bagmail@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/bag.png',
      price: '250',
      title: 'Bolso piel marca Hoss'
    },
    {
      description: 'Reloj de la marca Daniel Wellington usado sólo un mes. Ahora me han regalado otro que me gusta más y es muy parecido; por eso lo vendo. Su precio en tienda son 149 pero lo vendo por 100 euros. Es con la esfera blanca y la correa de piel marron. ',
      email: 'watchmail@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/watch.png',
      price: '100',
      title: 'Reloj de Daniel Wellington'
    },
    {
      description: 'Coche antiguo americano de color marrón. Se tiene que cambiar el motor pero aparte de eso todo funciona correctamente. Interior de piel clara. Ideal para coleccionistas',
      email: 'carmail@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/car.png',
      price: '210000',
      title: 'Coche antiguo americano'
    },
    {
      description: 'Barbacoa en buen estado. La he usado pocas veces y está casi nueva. Ideal para fiestas y celebraciones',
      email: 'barbecue@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/barbecue.png',
      price: '120',
      title: 'Barbacoa'
    }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteComponent ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatInputModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatIconModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call apply favorite in order to emit an event to the parent', async () => {
    component.wallapopDataFiltered = data
    component.dataSource = new MatTableDataSource<IWallapopCleanData>(data);
    const setFavoriteSpy = spyOn(component.setFavorite, 'emit');
    const button = await loader.getHarness<MatButtonHarness>(MatButtonHarness);
    await button.click();
    expect(setFavoriteSpy).toHaveBeenCalled();
  });

  it('should search iphone and get in the list one entry', async () => {
    component.wallapopDataFiltered = data
    component.dataSource = new MatTableDataSource<IWallapopCleanData>(data);
    const inputField = await loader.getHarness<MatInputHarness>(MatInputHarness);
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    await inputField.setValue('iphone');
    const rows = await table.getRows();
    expect(rows.length).toBe(1);
  });

  it('should search abcde and noDataFound should be true ', async () => {
    component.wallapopDataFiltered = data
    component.dataSource = new MatTableDataSource<IWallapopCleanData>(data);
    const inputField = await loader.getHarness<MatInputHarness>(MatInputHarness);
    await inputField.setValue('iphone');
    expect(component.noDataFound).toBeTruthy();
  });
});
