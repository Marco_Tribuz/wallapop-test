import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {FavoriteComponent} from '../favorite/favorite.component';
import {TableService} from './table.service';
import {MatSnackBar} from "@angular/material/snack-bar";

export interface IWallapopRawData {
  title: string;
  description: string;
  price: string;
  email: string;
  image: string;
}

export interface IWallapopCleanData {
  title: string;
  description: string;
  price: string;
  email: string;
  image: string;
  favorite: boolean;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  wallapopData: IWallapopCleanData[];
  loadingData: boolean; // to show a spinner
  header: string[] = ['title', 'description', 'price', 'email', 'image', 'favorite'];
  dataSource: MatTableDataSource<IWallapopCleanData>;
  dialogRef: MatDialogRef<FavoriteComponent>;
  noDataFound: boolean;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private tableService: TableService, public dialog: MatDialog, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.loadData();
  }

  async loadData(): Promise<void> {
    try {
      this.loadingData = true;
      console.log('Load data');
      const list: IWallapopRawData[] = await this.tableService.getList();
      if (list && list.length > 0) {
        this.wallapopData = list.map(item => {
          return {
            title: item.title,
            description: item.description,
            price: item.price,
            email: item.email,
            image: item.image,
            favorite: false,
          };
        });
        this.dataSource = new MatTableDataSource<IWallapopCleanData>(this.wallapopData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loadingData = false;
      }else {
        this.loadingData = false;
        this.openSnackbar()
      }
    }catch (e) {
      console.error('[TableComponent][loadData] An error occurred loading the page', e);
      this.loadingData = false;
      this.openSnackbar()
    }
  }

  searchItem(event: Event): void {
    const searchValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = searchValue.trim().toLocaleLowerCase();
    if (this.dataSource.paginator) { // reset paginator
      this.dataSource.paginator.firstPage();
    }
    this.noDataFound = this.dataSource && this.dataSource.filteredData && this.dataSource.filteredData.length > 0;
  }

  applyFavorite(item: IWallapopCleanData): void {
    item.favorite = !item.favorite;
  }

  showDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '800px';
    const filterData: IWallapopCleanData[] = this.wallapopData.filter(item => item.favorite === true);
    dialogConfig.data = filterData;
    this.dialogRef = this.dialog.open(FavoriteComponent, dialogConfig);
    this.dialogRef.componentInstance.setFavorite.subscribe((event: IWallapopCleanData) => {
      this.applyFavorite(event);
      const update: IWallapopCleanData[] = this.wallapopData.filter(item => item.favorite === true);
      this.dialogRef.componentInstance.wallapopDataFiltered = update;
      this.dialogRef.componentInstance.dataSource = new MatTableDataSource<IWallapopCleanData>(update);
    });
  }

  private openSnackbar (): void {
    const snackBarRef = this._snackBar.open('Something Wrong', 'Retry', {
      duration: 5000,
    });
    snackBarRef.onAction().subscribe(()=> location.reload());
  }

  showBadge (): boolean {
    return !(this.wallapopData && this.wallapopData.filter(item => item.favorite === true).length > 0)
  }

  favoriteItems (): string {
    if (this.wallapopData && this.wallapopData.filter(item => item.favorite === true).length > 0)
      return (this.wallapopData.filter(item => item.favorite === true).length).toString()
  }

}
