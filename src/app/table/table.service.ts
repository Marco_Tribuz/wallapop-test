import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {IWallapopRawData} from './table.component';

@Injectable({
  providedIn: 'root'
})

export class TableService {
  API_URL = 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/items.json';
  list: any;
  constructor(private http: HttpClient) {}

  async getList(): Promise<IWallapopRawData[]> {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Access-Control-Allow-Origin':'*'
        })
      };

      this.list = await this.http.get(this.API_URL, httpOptions).toPromise();
      return this.list.items;
    }catch (e) {
      console.error('[TableComponent][getList] Host offline', e);
      throw new Error('Host offline');
    }
  }
}
