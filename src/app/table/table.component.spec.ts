import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {TableComponent} from './table.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {TableService} from './table.service';
import {MatTableHarness} from '@angular/material/table/testing';
import {HarnessLoader} from '@angular/cdk/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {IWallapopCleanData} from './table.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {MatInputHarness} from '@angular/material/input/testing';
import {MatButtonHarness} from '@angular/material/button/testing';
import {of} from 'rxjs';
import {FavoriteComponent} from '../favorite/favorite.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let loader: HarnessLoader;

  const data: IWallapopCleanData[] = [
    {
      title: 'iPhone 6S Oro',
      description: 'Vendo un iPhone 6 S color Oro nuevo y sin estrenar. Me han dado uno en el trabajo y no necesito el que me compré. En tienda lo encuentras por 749 euros y yo lo vendo por 740. Las descripciones las puedes encontrar en la web de apple. Esta libre.',
      email: 'iphonemail@wallapop.com',
      favorite: false,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png',
      price: '740',
    },
    {
      description: 'Cámara clásica de fotos Polaroid, modelo 635. Las fotos son a super color. Está en perfectas condiciones y es fantástica para coleccionistas. Se necesitan carretes instax 20 para hacer fotos. Tamaño M.',
      email: 'cameramail@wallapop.com',
      favorite: false,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/camera.png',
      price: '50',
      title: 'Polaroid 635'
    },
    {
      description: 'Vendo bolso de piel marrón grande de la marca Hoss. Lo compré hace dos temporadas. Esta en perfectas condiciones, siempre se ha guardado en bolsa de tela para su conservación. Precio original de 400 euros. Lo vendo por 250 porque ya casi no me lo pongo. Tiene varios compartimentos dentro.',
      email: 'bagmail@wallapop.com',
      favorite: false,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/bag.png',
      price: '250',
      title: 'Bolso piel marca Hoss'
    },
    {
      description: 'Reloj de la marca Daniel Wellington usado sólo un mes. Ahora me han regalado otro que me gusta más y es muy parecido; por eso lo vendo. Su precio en tienda son 149 pero lo vendo por 100 euros. Es con la esfera blanca y la correa de piel marron. ',
      email: 'watchmail@wallapop.com',
      favorite: false,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/watch.png',
      price: '100',
      title: 'Reloj de Daniel Wellington'
    },
    {
      description: 'Coche antiguo americano de color marrón. Se tiene que cambiar el motor pero aparte de eso todo funciona correctamente. Interior de piel clara. Ideal para coleccionistas',
      email: 'carmail@wallapop.com',
      favorite: false,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/car.png',
      price: '210000',
      title: 'Coche antiguo americano'
    },
    {
      description: 'Barbacoa en buen estado. La he usado pocas veces y está casi nueva. Ideal para fiestas y celebraciones',
      email: 'barbecue@wallapop.com',
      favorite: false,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/barbecue.png',
      price: '120',
      title: 'Barbacoa'
    }];

  class TableServiceMock {
    getList() {
      return data;
    }
  }

  class MdDialogMock {
    // When the component calls this.dialog.open(...) we'll return an object
    // with an afterClosed method that allows to subscribe to the dialog result observable.
    open() {
      return {
        afterClosed: () => of()
      };
    }
  }

  beforeEach(async( () => {
    TestBed.configureTestingModule({
      declarations: [ TableComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        HttpClientTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatInputModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatIconModule,
        MatDialogModule,
        MatSnackBarModule
      ],
      providers: [{ provide: MatDialog, useClass: MdDialogMock },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: FavoriteComponent, useValue: {} },
        {provide: TableService, useValue: new TableServiceMock()}]
    })
    .compileComponents();
  }));

  beforeEach( () => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the correct table headings', async () => {
    const expectedHeadings = ['Title', 'Description', 'Price', 'Email', '', ''];
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const headerRows = await table.getHeaderRows();
    expect(await headerRows[0].getCellTextByIndex()).toEqual(expectedHeadings);
  });

  it('should initially display 0 rows in the table', async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const rows = await table.getRows();
    expect(rows.length).toBe(5);
  });

  it('should initially display 5 rows in the table', async () => {
    await component.loadData();
    fixture.detectChanges();
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const rows = await table.getRows();
    expect(rows.length).toBe(5);
  });

  it ('should hide the loader after init', async () => {
    await component.loadData();
    expect(component.loadingData).toBeFalsy();
  });

  it ('should search', async () => {
    const inputField = await loader.getHarness<MatInputHarness>(MatInputHarness);
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    await inputField.setValue('iphone');
    const rows = await table.getRows();
    expect(rows.length).toBe(1);
  });

  it ('should click on button and open the dialog', async () => {
    const button = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({selector: '#showDialog'}));
    await button.click();
    expect(component.dialogRef).toBeTruthy();
  });

  it ('apply favorite should change the status of element', () => {
    const item: IWallapopCleanData = {
      description: 'Barbacoa en buen estado. La he usado pocas veces y está casi nueva. Ideal para fiestas y celebraciones',
      email: 'barbecue@wallapop.com',
      favorite: false,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/barbecue.png',
      price: '120',
      title: 'Barbacoa'
    };
    component.applyFavorite(item);
    expect(item.favorite).toBeTruthy();
  });

  it ('should get 1 item if there is 1 item in favorite ', () => {
    const item: IWallapopCleanData = {
      description: 'Barbacoa en buen estado. La he usado pocas veces y está casi nueva. Ideal para fiestas y celebraciones',
      email: 'barbecue@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/barbecue.png',
      price: '120',
      title: 'Barbacoa'
    };
    component.wallapopData = [item]
    const numberItems = component.favoriteItems();
    fixture.detectChanges();
    expect(numberItems).toBe("1")
  });

  it ('should show number when favorite is > 1 ', () => {
    const item: IWallapopCleanData = {
      description: 'Barbacoa en buen estado. La he usado pocas veces y está casi nueva. Ideal para fiestas y celebraciones',
      email: 'barbecue@wallapop.com',
      favorite: true,
      image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/barbecue.png',
      price: '120',
      title: 'Barbacoa'
    };
    component.wallapopData = [item]
    const isHide = component.showBadge();
    fixture.detectChanges();
    expect(isHide).toBeFalsy()
  });

});
