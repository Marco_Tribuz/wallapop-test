# Wallapop Angular Test - Item Manager
## Development server

To correctly view the application, first run `npm install` in the root of project, then run `ng serve` for a dev server and navigate to `http://localhost:4200/`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running test coverage

Run `ng test --code-coverage` to execute the unit tests coverage

## Description

For the purpose of the test I used the most up-to-date version available of the frameworks that I chose: Angular version 9.1.6 and Angular Material 9.2.3. I chose Angular Material because it offers a high level API test.
This component makes tests easier to read, more robust and less likely to break when updating the framework. For the responsive design instead I used Angular flex 9.0.0 that provides a sophisticated layout API using Flexbox CSS + mediaQuery. On top it has a responsive engine.

I implemented unit tests for a total coverage of 80% of the code.
